# dnsresolv.py

Resolve domains and ips


## Installation

```
pip install dnsresolv
```

## Examples

```shell
echo "www.google.com" | dnsresolvpy
www.google.com 142.250.203.100
```
