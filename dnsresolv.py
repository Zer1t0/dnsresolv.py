#!/usr/bin/env python3
import dnsresolv

if __name__ == '__main__':
    exit(dnsresolv.main())
